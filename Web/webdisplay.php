<?php

$shopID = $_GET['shop_id'];
include 'conn.php';

    function getProduct()
    {
        global $con; //global is to access $con, connection variable outside the function
        global $shopID;
        $get_product = "SELECT * FROM table_product WHERE shop_id = '$shopID'"; //SQL statement for selecting data from database
        $run_product = mysqli_query($con, $get_product); //perform query on a database

        while ($row_product = mysqli_fetch_array($run_product)) {
            //fetch data by row
            $product_name = $row_product['product_name'];
            $product_price = $row_product['product_price']; //asign data to a variable
            $product_id = $row_product['product_id'];
            $shop_id = $row_product['shop_id'];
          //  $new_product_price = vsprintf('%d', sscanf($product_price, '%3d'));
            $img_dustbin_url = 'resources/ic_delete_grey600_36dp.png';

            echo'<tr>';
            echo "<td> $product_name </td>";
            echo '<td>RM'.number_format((float) $product_price, 2, '.', '').'</td>';
            echo '<td><a href=webeditproduct.php?product_id='.$product_id.'><img src="http://i.imgur.com/BZ84ztm.png" title="source: imgur.com" width="20" height="20" /></a></td>';
            echo '<td><a href=webdeleteproduct.php?product_id='.$product_id.'&shop_id='.$shop_id.'><img src="http://i.imgur.com/EmewImd.png" title="source: imgur.com" width="20" height="20" /></a></td>';

            //echo "<td align = 'center' class='td-vertical-align-center'>".'<a href= delete.php?id='.$row_person['personID']." role='button'><img src=''></a>".'</td>';
            //echo "<td align = 'center' class='td-vertical-align-center'>"."<a class='btn btn-primary btn-sm' href= delete.php?id=".$row_person['personID']." role='button'>Update</a>".'</td>';
        }
    }

?>

<!doctype html>

<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">

	<title>Product</title>
</head>

<body>
	<div class="container">

<!--<a href="http://imgur.com/EmewImd"><img src="http://i.imgur.com/EmewImd.png" title="source: imgur.com" width='20' height='20' /></a>-->
  <table class="table">
    <thead>
      <tr>
        <th class="th-align-center">Product name</th>
        <th class="th-align-center">Price</th>
        <th class="th-align-center"></th>
        <th class="th-align-center"></th>
      </tr>
    </thead>
    <tbody>
        <?php getProduct() ?>
    </tbody>
  </table>
</div>

	<script src="bootstrap/js/jquery.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
</body>
</html>
