<?php

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    $url = $_GET['qr_url'];

    require_once 'conn.php';

    $sql = "SELECT * FROM table_shop WHERE (shop_url LIKE '$url' or shop_name LIKE '$url')";

    $r = mysqli_query($con, $sql);

    $res = mysqli_fetch_array($r);
    if (!$res) {
        printf("Error: %s\n", mysqli_error($con));
        exit();
    }

    $result = array();

    array_push($result, array(
      'id' => $res['shop_id'],
      'shopname' => $res['shop_name'],
      'shopdescription' => $res['shop_description'] )
    );

    echo json_encode(array('result' => $result));

    mysqli_close($con);
}
