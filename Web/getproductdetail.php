<?php

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    $shopID = $_GET['shop_id'];
    $productBarcode = $_GET['product_barcode'];
    $productBarcodeFormat = $_GET['product_barcode_format'];

    require_once 'conn.php';

    $sql = "SELECT * FROM table_product WHERE (shop_id LIKE '$shopID' AND product_barcode LIKE '$productBarcode' AND product_barcode_format LIKE '$productBarcodeFormat')";

    $r = mysqli_query($con, $sql);

    $res = mysqli_fetch_array($r);
    if (!$res) {
        printf("Error: %s\n", mysqli_error($con));
        exit();
    }

    $result = array();

    array_push($result, array(
      'product_price' => $res['product_price'],
      'product_name' => $res['product_name'],
      'product_description' => $res['product_description'],
      'product_barcode' => $res['product_barcode'],
      'product_barcode_format' => $res['product_barcode_format'],
 )
    );

    echo json_encode(array('result' => $result));

    mysqli_close($con);
}
