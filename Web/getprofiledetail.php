<?php

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    $adminID = $_GET['admin_id'];

    require_once 'conn.php';

    $sql = "SELECT * FROM table_admin WHERE admin_id LIKE '$adminID'";

    $r = mysqli_query($con, $sql);

    $res = mysqli_fetch_array($r);
    if (!$res) {
        printf("Error: %s\n", mysqli_error($con));
        exit();
    }

    $result = array();

    array_push($result, array(
      'admin_username' => $res['admin_username'],
      'admin_email' => $res['admin_email'],
      'admin_fname' => $res['admin_fname'],
      'admin_lname' => $res['admin_lname'],
 )
    );

    echo json_encode(array('result' => $result));

    mysqli_close($con);
}
