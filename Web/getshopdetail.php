<?php

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    $shopID = $_GET['shop_id'];

    require_once 'conn.php';

    $sql = "SELECT * FROM table_shop WHERE shop_id LIKE '$shopID'";

    $r = mysqli_query($con, $sql);

    $res = mysqli_fetch_array($r);
    if (!$res) {
        printf("Error: %s\n", mysqli_error($con));
        exit();
    }

    $result = array();

    array_push($result, array(
      'shop_name' => $res['shop_name'],
      'shop_description' => $res['shop_description'],
      'shop_url' => $res['shop_url'],
 )
    );

    echo json_encode(array('result' => $result));

    mysqli_close($con);
}
