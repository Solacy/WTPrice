<?php

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    $email = $_GET['email'];

    require_once 'conn.php';

    $sql = "SELECT * FROM table_admin WHERE admin_email LIKE '$email'";

    $r = mysqli_query($con, $sql);

    $res = mysqli_fetch_array($r);
    if (!$res) {
        printf("Error: %s\n", mysqli_error($con));
        exit();
    }

    $result = array();

    array_push($result, array(
      'id' => $res['admin_id'],
      'username' => $res['admin_username'],
      'fname' => $res['admin_fname'],
      'lname' => $res['admin_lname'],
      'shopid' => $res['shop_id'],
      'email' => $res['admin_email'], )
    );

    echo json_encode(array('result' => $result));

    mysqli_close($con);
}
