<?php
include 'conn.php';
$product_id = $_GET['product_id'];

        $get_product = "SELECT * FROM table_product WHERE product_id = '$product_id'";
        $run_product = mysqli_query($con, $get_product);
        while ($row_product = mysqli_fetch_array($run_product)) {
            $product_name = $row_product['product_name'];
            $product_price = $row_product['product_price'];
            $product_description = $row_product['product_description'];
            $product_barcode = $row_product['product_barcode'];
            $shop_id = $row_product['shop_id'];
            $product_barcode_format = $row_product['product_barcode_format'];
        }

?>

<!doctype html>

<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">

	<title>Product Info</title>
</head>

<body>

    <div>
    	<div class="row">
       		<div class="col-sm-10 col-sm-push-2">
        	<div class="panel panel-default">
  				<!-- Default panel contents -->
          <a href="webdisplay.php?shop_id=<?php echo $shop_id ?>"><button type="submit" class="btn btn-info btn-lg btn-block"name="return">Return</button></a>

  				<div class="panel-heading" align="center"><h4><strong>Update Product</strong></h4></div>

					<form action="webeditproduct.php?product_id=<?php echo $product_id ?>" method="POST" enctype="multipart/form-data">
  					<!-- Table -->
  					<table class="table">
              <tr>
                  <td class="col-md-3" align="right">Barcode: </td>
                    <td><?php echo $product_barcode ?></td>
                </tr>

                <tr>
                    <td class="col-md-3" align="right">Format: </td>
                    <td><?php echo $product_barcode_format ?></td>
                  </tr>

                        	<tr>
                            	<td class="col-md-3" align="right">Product Name: </td>
                                <td><input type="text" class="form-control" value="<?php echo $product_name ?>" name="product_name" required></td>
                            </tr>

                            <tr>
                            	<td align="right">Product Price: </td>
                                <td><input type="number" step="0.01" class="form-control" value="<?php echo $product_price ?>" name="product_price" required></td>
                            </tr>
                            <tr>
                            	<td align="right">Product Description: </td>
                                <td><input type="text" class="form-control" value="<?php echo $product_description ?>" name="product_description" required></td>
                            </tr>




  					</table>
                    <button type="submit" class="btn btn-info btn-lg btn-block" name="update_post">Submit</button>

                   </form>
				</div>
        	</div><!--end of content-->


    	</div><!--end of 1st row-->


        <?php


    if (isset($_POST['update_post'])) {
        global $con;
        global $product_id;
        global $shop_id;
        $product_name = $_POST['product_name'];
        $product_price = $_POST['product_price'];
        $product_description = $_POST['product_description'];

        $update_product = "UPDATE table_product SET product_name = '$product_name' , product_price = '$product_price', product_description='$product_description' WHERE product_id = '$product_id'";

        $update_product_success = mysqli_query($con, $update_product);

        header("Location: webdisplay.php?shop_id=$shop_id");
    }

?>

	<script src="bootstrap/js/jquery.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
</body>
</html>
