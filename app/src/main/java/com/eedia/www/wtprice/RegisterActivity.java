package com.eedia.www.wtprice;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText editTextUsername;
    private EditText editTextPassword;
    private EditText editTextEmail;
    private EditText editTextFname;
    private EditText editTextLname;
    private EditText editTextShopName;

    private Button buttonRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        editTextUsername = (EditText) findViewById(R.id.content_register_et_username);
        editTextPassword = (EditText) findViewById(R.id.content_register_et_password);
        editTextEmail = (EditText) findViewById(R.id.content_register_et_email);
        editTextFname = (EditText) findViewById(R.id.content_register_et_fname);
        editTextLname = (EditText) findViewById(R.id.content_register_et_lname);
        editTextShopName = (EditText) findViewById(R.id.content_register_et_shop_name);

        buttonRegister = (Button) findViewById(R.id.content_register_btn_signup);

        buttonRegister.setOnClickListener(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void registerAdmin() {
        final String username = editTextUsername.getText().toString().trim();
        final String password = editTextPassword.getText().toString().trim();
        final String email = editTextEmail.getText().toString().trim();
        final String fname = editTextFname.getText().toString().trim();
        final String lname = editTextLname.getText().toString().trim();
        final String shopName = editTextShopName.getText().toString().trim();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.REGISTER_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Toast.makeText(RegisterActivity.this, response, Toast.LENGTH_LONG).show();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(RegisterActivity.this, error.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(Config.KEY_USERNAME_REGISTER, username);
                params.put(Config.KEY_PASSWORD_REGISTER, password);
                params.put(Config.KEY_EMAIL_REGISTER, email);
                params.put(Config.KEY_FNAME_REGISTER, fname);
                params.put(Config.KEY_LNAME_REGISTER, lname);
                params.put(Config.KEY_SHOP_NAME_REGISTER, shopName);
                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        if (v == buttonRegister) {
            registerAdmin();
        }
    }
}
