package com.eedia.www.wtprice;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

public class EditProductActivity extends AppCompatActivity {

    private TextView textViewBarcode;
    private TextView textViewBarcodeFormat;

    private EditText editTextProductName;
    private EditText editTextProductPrice;
    private EditText editTextProductDesc;

    private Button buttonUpdate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_product);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        textViewBarcode = (TextView) findViewById(R.id.content_edit_product_tv_barcode);
        textViewBarcodeFormat = (TextView) findViewById(R.id.content_edit_product_tv_barcode_format);

        editTextProductName = (EditText) findViewById(R.id.content_edit_product_et_product_name);
        editTextProductPrice = (EditText) findViewById(R.id.content_edit_product_et_product_price);
        editTextProductDesc = (EditText) findViewById(R.id.content_edit_product_et_product_desc);

        buttonUpdate = (Button) findViewById(R.id.content_edit_product_button_update);

        buttonUpdate.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                updateProduct();
            }
        });

        SharedPreferences printsharedPreferences = getSharedPreferences(Config.SHARED_PREF_ADMIN, Context.MODE_PRIVATE);
        final int shopID = printsharedPreferences.getInt(Config.SHOPID_ADMIN, -1);

        Intent getBarcodeDetail = getIntent();
        final String barcode = getBarcodeDetail.getStringExtra("barcode");
        final String barcode_format = getBarcodeDetail.getStringExtra("barcode_format");


        String url = Config.GET_PRODUCT_DETAIL_URL + shopID + Config.GET_PRODUCT_DETAIL_URL_2 + barcode + Config.GET_PRODUCT_DETAIL_URL_3 + barcode_format;

        StringRequest stringRequest = new StringRequest(url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                String productName = "";
                String productPrice = "";
                String productDesc = "";
                String productBarcode = "";
                String productBarcodeFormat = "";


                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray result = jsonObject.getJSONArray(Config.JSON_ARRAY);
                    JSONObject getproductdetail = result.getJSONObject(0);
                    productName = getproductdetail.getString(Config.GET_PRODUCT_DETAIL_NAME);
                    productPrice = getproductdetail.getString(Config.GET_PRODUCT_DETAIL_PRICE);
                    productDesc = getproductdetail.getString(Config.GET_PRODUCT_DETAIL_DESC);
                    productBarcode = getproductdetail.getString(Config.GET_PRODUCT_DETAIL_BARCODE);
                    productBarcodeFormat = getproductdetail.getString(Config.GET_PRODUCT_DETAIL_BARCODE_FORMAT);

                    editTextProductName.setText(productName);
                    editTextProductPrice.setText(productPrice);
                    editTextProductDesc.setText(productDesc);

                    textViewBarcode.setText(productBarcode);
                    textViewBarcodeFormat.setText(productBarcodeFormat);

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(EditProductActivity.this, error.getMessage().toString(), Toast.LENGTH_LONG).show();
                    }
                });

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void updateProduct(){
        SharedPreferences printsharedPreferences = getSharedPreferences(Config.SHARED_PREF_ADMIN, Context.MODE_PRIVATE);
        final int shopID = printsharedPreferences.getInt(Config.SHOPID_ADMIN, -1);
        final String productName = editTextProductName.getText().toString().trim();
        final String productPrice = editTextProductPrice.getText().toString().trim();
        final String productDesc = editTextProductDesc.getText().toString();
        final String productBarcode = textViewBarcode.getText().toString().trim();
        final String productBarcodeFormat = textViewBarcodeFormat.getText().toString().trim();

        final String strShopID = String.valueOf(shopID);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.EDIT_PRODUCT_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Toast.makeText(EditProductActivity.this, response, Toast.LENGTH_LONG).show();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(EditProductActivity.this, error.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(Config.KEY_PRODUCT_NAME, productName);
                params.put(Config.KEY_PRODUCT_PRICE, productPrice);
                params.put(Config.KEY_PRODUCT_DESC, productDesc);
                params.put(Config.KEY_PRODUCT_SHOP_ID, strShopID);
                params.put(Config.KEY_BARCODE, productBarcode);
                params.put(Config.KEY_BARCODE_FORMAT,productBarcodeFormat);
                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

        Intent intent = new Intent(this, AdminActivity.class);
        startActivity(intent);


    }

}
