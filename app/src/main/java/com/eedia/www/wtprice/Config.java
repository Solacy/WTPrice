package com.eedia.www.wtprice;

/**
 * Created by hafiz on 10/10/2016.
 */

public class Config {

    ///////////////////////////Shared preference//////////////////////////////

    public static final String LOGIN_URL = "http://yourowndomain.com/login.php";

    //Keys for email and password as defined in our $_POST['key'] in login.php
    public static final String KEY_EMAIL_SESSION = "email";
    public static final String KEY_PASSWORD_SESSION = "password";

    //If server response is equal to this that means login is successful
    public static final String LOGIN_SUCCESS = "success";

    //Keys for Sharedpreferences
    //This would be the name of our shared preferences
    public static final String SHARED_PREF_NAME = "WTPsession";

    //This would be used to store the email of current logged in user
    public static final String EMAIL_SESSION = "email";
    //We will use this to store the boolean in sharedpreference to track user is loggedin or not
    public static final String LOGGEDIN_SESSION = "loggedin";

    //shared preference for shopname
    public static final String SHARED_PREF_SHOPNAME = "ShopnameSession";
    public static final String SHOP_NAME_SESSION = "shopname";

    //This would be the name of our shared preferences
    public static final String SHARED_PREF_ADMIN = "WTPadmin";

    public static final String GETID_URL = "http://yourowndomain.com/getid.php?email=";
    public static final String KEY_ID = "id";
    public static final String KEY_USERNAME = "username";
    public static final String KEY_FIRSTNAME = "fname";
    public static final String KEY_LASTNAME = "lname";
    public static final String KEY_SHOPID = "shopid";
    public static final String KEY_ADMIN = "email";

    public static final String ID_ADMIN = "id";
    public static final String USERNAME_ADMIN = "username";
    public static final String FIRSTNAME_ADMIN = "fname";
    public static final String LASTNAME_ADMIN = "lname";
    public static final String SHOPID_ADMIN = "shopid";
    public static final String EMAIL_ADMIN = "email";
    public static final String JSON_ARRAY = "result";

    //Register user
    public static final String REGISTER_URL = "http://yourowndomain.com/register.php";
    public static final String KEY_USERNAME_REGISTER = "username";
    public static final String KEY_PASSWORD_REGISTER = "password";
    public static final String KEY_EMAIL_REGISTER = "email";
    public static final String KEY_FNAME_REGISTER = "fname";
    public static final String KEY_LNAME_REGISTER = "lname";
    public static final String KEY_SHOP_NAME_REGISTER = "shopname";

    //To call ZXing scanner
    static final String ACTION_SCAN = "com.google.zxing.client.android.SCAN";

    //To get shop name and display on scanproductactivity
    public static final String GET_SHOP_NAME_URL = "http://yourowndomain.com/getshopname.php?qr_url=";
    public static final String KEY_GETSHOPID = "id";
    public static final String KEY_GETSHOPNAME = "shopname";
    public static final String KEY_GETSHOPDESCRIPTION = "shopdescription";

    //to pass get pass barcode information and get product detail from database
    public static final String GET_PRODUCT_DETAIL_URL = "http://yourowndomain.com/getproductdetail.php?shop_id=";
    public static final String GET_PRODUCT_DETAIL_URL_2 = "&product_barcode=";
    public static final String GET_PRODUCT_DETAIL_URL_3 = "&product_barcode_format=";
    public static final String GET_PRODUCT_DETAIL_NAME = "product_name";
    public static final String GET_PRODUCT_DETAIL_PRICE = "product_price";
    public static final String GET_PRODUCT_DETAIL_DESC = "product_description";
    public static final String GET_PRODUCT_DETAIL_BARCODE = "product_barcode";
    public static final String GET_PRODUCT_DETAIL_BARCODE_FORMAT = "product_barcode_format";

    //add product
    public static final String ADD_PRODUCT_URL = "http://yourowndomain.com/addproduct.php";
    public static final String KEY_PRODUCT_NAME = "productname";
    public static final String KEY_PRODUCT_PRICE = "productprice";
    public static final String KEY_PRODUCT_DESC = "productdesc";
    public static final String KEY_BARCODE = "barcode";
    public static final String KEY_BARCODE_FORMAT = "barcodeformat";
    public static final String KEY_PRODUCT_SHOP_ID = "shopid";

    //edit product
    public static final String EDIT_PRODUCT_URL = "http://yourowndomain.com/updateproduct.php";

    //delete product
    public static final String DELETE_PRODUCT_URL = "http://yourowndomain.com/deleteproduct.php";

    //webdisplay
    public static final String WEB_PRODUCT_DISPLAY_URL = "http://yourowndomain.com/webdisplay.php?shop_id=";

    //get shop detail
    public static final String GET_SHOP_DETAIL_URL = "http://yourowndomain.com/getshopdetail.php?shop_id=";
    public static final String GET_SHOP_NAME = "shop_name";
    public static final String GET_SHOP_DESCRIPTION = "shop_description";
    public static final String GET_SHOP_QR = "shop_url";

    //update shop detail
    public static final String UPDATE_SHOP_URL = "http://yourowndomain.com/updateshop.php";
    public static final String KEY_SHOP_ID = "shopid";
    public static final String KEY_SHOP_DESCRIPTION = "shop_description";
    public static final String KEY_SHOP_QR = "shop_url";

    //get profile detail
    public static final String GET_PROFILE_DETAIL_URL = "http://yourowndomain.com/getprofiledetail.php?admin_id=";
    public static final String GET_PROFILE_USERNAME = "admin_username";
    public static final String GET_PROFILE_EMAIL = "admin_email";
    public static final String GET_PROFILE_FNAME = "admin_fname";
    public static final String GET_PROFILE_LNAME = "admin_lname";

    public static final String UPDATE_PROFILE_URL = "http://yourowndomain.com/updateprofile.php";
    public static final String KEY_PROFILE_ID = "admin_id";
    public static final String KEY_PROFILE_FNAME = "admin_fname";
    public static final String KEY_PROFILE_LNAME = "admin_lname";
    public static final String KEY_PROFILE_EMAIL = "admin_email";


}
