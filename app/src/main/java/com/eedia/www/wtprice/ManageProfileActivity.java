package com.eedia.www.wtprice;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ManageProfileActivity extends AppCompatActivity {

    private EditText editTextFName;
    private EditText editTextLName;
    private EditText editTextEmail;
    private TextView textViewUsername;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_profile);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        editTextFName = (EditText) findViewById(R.id.content_manage_profile_et_fname);
        editTextLName = (EditText) findViewById(R.id.content_manage_profile_et_lname);
        editTextEmail =  (EditText) findViewById(R.id.content_manage_profile_et_email);
        textViewUsername = (TextView) findViewById(R.id.content_manage_profile_tv_username);

        SharedPreferences printsharedPreferences = getSharedPreferences(Config.SHARED_PREF_ADMIN, Context.MODE_PRIVATE);
        final int adminID = printsharedPreferences.getInt(Config.ID_ADMIN, -1);

        String url = Config.GET_PROFILE_DETAIL_URL + adminID;

        StringRequest stringRequest = new StringRequest(url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                String username = "";
                String email = "";
                String fname = "";
                String lname = "";


                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray result = jsonObject.getJSONArray(Config.JSON_ARRAY);
                    JSONObject getprofiledetail = result.getJSONObject(0);
                    username = getprofiledetail.getString(Config.GET_PROFILE_USERNAME);
                    email = getprofiledetail.getString(Config.GET_PROFILE_EMAIL);
                    fname = getprofiledetail.getString(Config.GET_PROFILE_FNAME);
                    lname = getprofiledetail.getString(Config.GET_PROFILE_LNAME);

                    editTextFName.setText(fname);
                    editTextLName.setText(lname);
                    editTextEmail.setText(email);
                    textViewUsername.setText(username);

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(ManageProfileActivity.this, error.getMessage().toString(), Toast.LENGTH_LONG).show();
                    }
                });

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void updateProfile(View view){
        SharedPreferences printsharedPreferences = getSharedPreferences(Config.SHARED_PREF_ADMIN, Context.MODE_PRIVATE);
        final int adminID = printsharedPreferences.getInt(Config.ID_ADMIN, -1);
        final String adminFname = editTextFName.getText().toString();
        final String adminLname = editTextLName.getText().toString().trim();
        final String adminEmail = editTextEmail.getText().toString().trim();

        final String strAdminID = String.valueOf(adminID);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.UPDATE_PROFILE_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Toast.makeText(ManageProfileActivity.this, response, Toast.LENGTH_LONG).show();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(ManageProfileActivity.this, error.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(Config.KEY_PROFILE_ID, strAdminID);
                params.put(Config.KEY_PROFILE_FNAME, adminFname);
                params.put(Config.KEY_PROFILE_LNAME, adminLname);
                params.put(Config.KEY_PROFILE_EMAIL, adminEmail);
                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

        Intent intent = new Intent(this, AdminActivity.class);
        startActivity(intent);


    }


}
