package com.eedia.www.wtprice;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ManageShopActivity extends AppCompatActivity {

    private EditText editTextShopDescription;
    private TextView textViewScanQRCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_shop);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        editTextShopDescription = (EditText) findViewById(R.id.content_manage_shop_et_description);
        textViewScanQRCode = (TextView) findViewById(R.id.content_manage_shop_tv_scan_qr);

        SharedPreferences printsharedPreferences = getSharedPreferences(Config.SHARED_PREF_ADMIN, Context.MODE_PRIVATE);
        final int shopID = printsharedPreferences.getInt(Config.SHOPID_ADMIN, -1);

        String url = Config.GET_SHOP_DETAIL_URL + shopID;

        StringRequest stringRequest = new StringRequest(url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                String shopName = "";
                String shopDescription = "";
                String shopQR = "";


                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray result = jsonObject.getJSONArray(Config.JSON_ARRAY);
                    JSONObject getshopdetail = result.getJSONObject(0);
                    shopName = getshopdetail.getString(Config.GET_SHOP_NAME);
                    shopDescription = getshopdetail.getString(Config.GET_SHOP_DESCRIPTION);
                    shopQR = getshopdetail.getString(Config.GET_SHOP_QR);

                    editTextShopDescription.setText(shopDescription);
                    textViewScanQRCode.setText(shopQR);

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(ManageShopActivity.this, error.getMessage().toString(), Toast.LENGTH_LONG).show();
                    }
                });

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void scanQR(View v) {
        try {
            Intent intent = new Intent(Config.ACTION_SCAN);
            intent.putExtra("SCAN MODE", "QR_CODE_MODE");
            startActivityForResult(intent, 0);
        } catch (ActivityNotFoundException anfe) {
            showDialog(ManageShopActivity.this, "No Scanner Found", "Download a scanner code activity", "Yes", "No").show();
        }
    }

    private static AlertDialog showDialog(final Activity act, CharSequence title, CharSequence message, CharSequence buttonYes, CharSequence buttonNo) {
        AlertDialog.Builder downloadDialog = new AlertDialog.Builder(act);
        downloadDialog.setTitle(title);
        downloadDialog.setMessage(message);
        downloadDialog.setPositiveButton(buttonYes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Uri uri = Uri.parse("market://search?q=pname:" + "com.google.zxing.client.android");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                try {
                    act.startActivity(intent);
                } catch (ActivityNotFoundException anfe) {

                }
            }
        });
        downloadDialog.setNegativeButton(buttonNo, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        return downloadDialog.show();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {

        if (requestCode == 0) {
            if (resultCode == RESULT_OK) {
                String shopQR = intent.getStringExtra("SCAN_RESULT");
                textViewScanQRCode.setText(shopQR);

            }

        }

    }

    public void updateShop(View view){
        SharedPreferences printsharedPreferences = getSharedPreferences(Config.SHARED_PREF_ADMIN, Context.MODE_PRIVATE);
        final int shopID = printsharedPreferences.getInt(Config.SHOPID_ADMIN, -1);
        final String shopDescription = editTextShopDescription.getText().toString();
        final String shopQR = textViewScanQRCode.getText().toString().trim();

        final String strShopID = String.valueOf(shopID);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.UPDATE_SHOP_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Toast.makeText(ManageShopActivity.this, response, Toast.LENGTH_LONG).show();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(ManageShopActivity.this, error.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(Config.KEY_SHOP_ID, strShopID);
                params.put(Config.KEY_SHOP_DESCRIPTION, shopDescription);
                params.put(Config.KEY_SHOP_QR, shopQR);
                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

        Intent intent = new Intent(this, AdminActivity.class);
        startActivity(intent);


    }

}
