package com.eedia.www.wtprice;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

public class AddProductActivity extends AppCompatActivity {

    private TextView TVBarcode;
    private TextView TVBarcodeFormat;

    private EditText editTextProductName;
    private EditText editTextProductPrice;
    private EditText editTextProductDesc;

    private Button buttonSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_product);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        editTextProductName = (EditText) findViewById(R.id.content_add_product_et_product_name);
        editTextProductPrice = (EditText) findViewById(R.id.content_add_product_et_product_price);
        editTextProductDesc = (EditText) findViewById(R.id.content_add_product_et_product_desc);

        buttonSubmit = (Button) findViewById(R.id.content_add_product_button_submit);

        Intent getBarcodeDetail = getIntent();
        final String barcode = getBarcodeDetail.getStringExtra("barcode");
        final String barcode_format = getBarcodeDetail.getStringExtra("barcode_format");

        TVBarcode = (TextView) findViewById(R.id.content_add_product_tv_barcode);
        TVBarcodeFormat = (TextView) findViewById(R.id.content_add_product_tv_barcode_format);

        TVBarcode.setText(barcode);
        TVBarcodeFormat.setText(barcode_format);

        buttonSubmit.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                addProduct(barcode, barcode_format);
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void addProduct(final String barcode, final String barcodeFormat) {
        SharedPreferences printsharedPreferences = getSharedPreferences(Config.SHARED_PREF_ADMIN, Context.MODE_PRIVATE);
        final int shopID = printsharedPreferences.getInt(Config.SHOPID_ADMIN, -1);
        final String productName = editTextProductName.getText().toString().trim();
        final String productPrice = editTextProductPrice.getText().toString().trim();
        final String productDesc = editTextProductDesc.getText().toString();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.ADD_PRODUCT_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Toast.makeText(AddProductActivity.this, response, Toast.LENGTH_LONG).show();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(AddProductActivity.this, error.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(Config.KEY_PRODUCT_NAME, productName);
                params.put(Config.KEY_PRODUCT_PRICE, productPrice);
                params.put(Config.KEY_PRODUCT_DESC, productDesc);
                params.put(Config.KEY_PRODUCT_SHOP_ID, String.valueOf(shopID));
                params.put(Config.KEY_BARCODE, barcode);
                params.put(Config.KEY_BARCODE_FORMAT,barcodeFormat);
                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

        Intent intent = new Intent(this, AdminActivity.class);
        startActivity(intent);
    }


}
