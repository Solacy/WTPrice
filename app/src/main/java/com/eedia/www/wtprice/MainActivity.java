package com.eedia.www.wtprice;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

//volley
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

//json
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private EditText shopName;//to insert shopname
    private Button btnPassShopName;//proceed button after user enther shop name
    private TextView TVAdminEmail, TVAdminUsername;//to display admin email and username at navigation drawer

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //declare navigation view to be displayed at navigation drawer
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View header = navigationView.getHeaderView(0);
        TVAdminEmail = (TextView) header.findViewById(R.id.nav_header_main_tv_email);
        TVAdminUsername = (TextView) header.findViewById(R.id.nav_header_main_tv_username);
        SharedPreferences printsharedPreferences = getSharedPreferences(Config.SHARED_PREF_ADMIN, Context.MODE_PRIVATE);
        String currentusername = printsharedPreferences.getString(Config.USERNAME_ADMIN, "");
        String currentemail = printsharedPreferences.getString(Config.EMAIL_ADMIN, "");
        TVAdminEmail.setText(currentemail);
        TVAdminUsername.setText(currentusername);

        SharedPreferences printShopPreferences = getSharedPreferences(Config.SHARED_PREF_SHOPNAME, Context.MODE_PRIVATE);
        final String sessionShopName = printShopPreferences.getString(Config.SHOP_NAME_SESSION, "");


        shopName = (EditText) findViewById(R.id.content_main_et_shop_name);
        btnPassShopName = (Button) findViewById(R.id.content_main_btn_shop_name);

        shopName.setText(sessionShopName);

        btnPassShopName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String passShopName = shopName.getText().toString();
                GetShopData(passShopName);
            }
        });


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);
    }

    public void scanQR(View v) {
        try {
            Intent intent = new Intent(Config.ACTION_SCAN);
            intent.putExtra("SCAN MODE", "QR_CODE_MODE");
            startActivityForResult(intent, 0);
        } catch (ActivityNotFoundException anfe) {
            showDialog(MainActivity.this, "No Scanner Found", "Download a scanner code activity", "Yes", "No").show();
        }
    }

    private static AlertDialog showDialog(final Activity act, CharSequence title, CharSequence message, CharSequence buttonYes, CharSequence buttonNo) {
        AlertDialog.Builder downloadDialog = new AlertDialog.Builder(act);
        downloadDialog.setTitle(title);
        downloadDialog.setMessage(message);
        downloadDialog.setPositiveButton(buttonYes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Uri uri = Uri.parse("market://search?q=pname:" + "com.google.zxing.client.android");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                try {
                    act.startActivity(intent);
                } catch (ActivityNotFoundException anfe) {

                }
            }
        });
        downloadDialog.setNegativeButton(buttonNo, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        return downloadDialog.show();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {

        if (requestCode == 0) {
            if (resultCode == RESULT_OK) {
                String qr_url = intent.getStringExtra("SCAN_RESULT");

                GetShopData(qr_url);

            }

        }

    }

    public void GetShopData(String qr_url) {

        String url = Config.GET_SHOP_NAME_URL + qr_url;

        StringRequest stringRequest = new StringRequest(url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                int id = 0;
                String shopname = "";
                String shopdescription = "";

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray result = jsonObject.getJSONArray(Config.JSON_ARRAY);
                    JSONObject getid = result.getJSONObject(0);
                    id = getid.getInt(Config.KEY_GETSHOPID);
                    shopname = getid.getString(Config.KEY_GETSHOPNAME);
                    shopdescription = getid.getString(Config.KEY_GETSHOPDESCRIPTION);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (shopname != "") {

                    Toast.makeText(MainActivity.this, "Connected to shop successfully...", Toast.LENGTH_SHORT).show();
                    Intent passShopDetail = new Intent(MainActivity.this, ScanProductActivity.class);
                    passShopDetail.putExtra("shop_id", id);
                    passShopDetail.putExtra("shop_name", shopname);
                    passShopDetail.putExtra("shop_description", shopdescription);
                    startActivity(passShopDetail);
                } else {
                    Toast.makeText(MainActivity.this, "Invalid shop, try again...", Toast.LENGTH_SHORT).show();
                }


            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(MainActivity.this, error.getMessage().toString(), Toast.LENGTH_LONG).show();
                    }
                });

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_store) {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_information) {
            Intent intent = new Intent(this, DevProfileActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
