package com.eedia.www.wtprice;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class AdminActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {


    //Textview to show currently logged in user
    private TextView TVAdminEmail, TVAdminUsername;
    ProgressBar webViewProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //declare navigation view
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View header = navigationView.getHeaderView(0);
        /*View view=navigationView.inflateHeaderView(R.layout.nav_header_main);*/
        //Initializing textview
        TVAdminEmail = (TextView) header.findViewById(R.id.TVadminEmail);
        TVAdminUsername = (TextView) header.findViewById(R.id.TVadminUsername);

        getData();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ScanBarcodeAddProduct();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

    }

    private void logout() {
        //Creating an alert dialog to confirm logout
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("Are you sure you want to logout?");
        alertDialogBuilder.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {

                        //Getting out sharedpreferences
                        SharedPreferences session_preferences = getSharedPreferences(Config.SHARED_PREF_NAME, Context.MODE_PRIVATE);
                        //Getting editor
                        SharedPreferences.Editor session_editor = session_preferences.edit();
                        //Puting the value false for loggedin
                        session_editor.putBoolean(Config.LOGGEDIN_SESSION, false);
                        //Putting blank value to email
                        session_editor.putString(Config.EMAIL_SESSION, "");
                        //Saving the sharedpreferences
                        session_editor.apply();


                        //Getting out sharedpreferences
                        SharedPreferences admin_preferences = getSharedPreferences(Config.SHARED_PREF_ADMIN, Context.MODE_PRIVATE);
                        //Getting editor
                        SharedPreferences.Editor admin_editor = admin_preferences.edit();
                        admin_editor.putInt(Config.ID_ADMIN, -1);
                        admin_editor.putString(Config.USERNAME_ADMIN, "");
                        admin_editor.putString(Config.FIRSTNAME_ADMIN, "");
                        admin_editor.putString(Config.LASTNAME_ADMIN, "");
                        admin_editor.putString(Config.EMAIL_ADMIN, "");
                        admin_editor.putInt(Config.SHOPID_ADMIN, -1);

                        admin_editor.apply();


                        //Starting login activity
                        Intent intent = new Intent(AdminActivity.this, MainActivity.class);
                        startActivity(intent);
                    }
                });

        alertDialogBuilder.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {

                    }
                });

        //Showing the alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }

    private void getData() {
        SharedPreferences sharedPreferences = getSharedPreferences(Config.SHARED_PREF_NAME, Context.MODE_PRIVATE);
        String email = sharedPreferences.getString(Config.EMAIL_SESSION, "Not Available");
        String url = Config.GETID_URL + email;

        StringRequest stringRequest = new StringRequest(url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                showJSON(response);
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(AdminActivity.this, error.getMessage().toString(), Toast.LENGTH_LONG).show();
                    }
                });

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void showJSON(String response) {
        int id = 0;
        int shopid = 0;
        String username = "";
        String fname = "";
        String lname = "";
        String email = "";

        try {
            JSONObject jsonObject = new JSONObject(response);
            JSONArray result = jsonObject.getJSONArray(Config.JSON_ARRAY);
            JSONObject getid = result.getJSONObject(0);
            id = getid.getInt(Config.KEY_ID);
            username = getid.getString(Config.KEY_USERNAME);
            fname = getid.getString(Config.KEY_FIRSTNAME);
            lname = getid.getString(Config.KEY_LASTNAME);
            shopid = getid.getInt(Config.KEY_SHOPID);
            email = getid.getString(Config.KEY_ADMIN);
            //Toast.makeText(this, id + username + fname+lname+shopname+email, Toast.LENGTH_SHORT).show();

        } catch (JSONException e) {
            e.printStackTrace();
        }
        SharedPreferences sharedPreferences = AdminActivity.this.getSharedPreferences(Config.SHARED_PREF_ADMIN, Context.MODE_PRIVATE);
        //Creating editor to store values to shared preferences
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(Config.ID_ADMIN, id);
        editor.putString(Config.USERNAME_ADMIN, username);
        editor.putString(Config.FIRSTNAME_ADMIN, fname);
        editor.putString(Config.LASTNAME_ADMIN, lname);
        editor.putInt(Config.SHOPID_ADMIN, shopid);
        editor.putString(Config.EMAIL_ADMIN, email);
        editor.apply();

        SharedPreferences printsharedPreferences = getSharedPreferences(Config.SHARED_PREF_ADMIN, Context.MODE_PRIVATE);
        String currentusername = printsharedPreferences.getString(Config.USERNAME_ADMIN, "Not available");
        String currentemail = printsharedPreferences.getString(Config.EMAIL_ADMIN, "Not available");
        int shopID = printsharedPreferences.getInt(Config.SHOPID_ADMIN, -1);
        webDisplay(shopID);

        //Showing the current logged in email to textview
        TVAdminUsername.setText(currentusername);
        TVAdminEmail.setText(currentemail);

    }

    public void ScanBarcodeAddProduct() {
        try {
            Intent intent = new Intent(Config.ACTION_SCAN);
            intent.putExtra("SCAN MODE", "PRODUCT_MODE");
            startActivityForResult(intent, 0);
        } catch (ActivityNotFoundException anfe) {
            showDialog(AdminActivity.this, "No Scanner Found", "Download a scanner code activity", "Yes", "No").show();
        }
    }

    public void ScanBarcodeEditProduct() {
        try {
            Intent intent = new Intent(Config.ACTION_SCAN);
            intent.putExtra("SCAN MODE", "PRODUCT_MODE");
            startActivityForResult(intent, 1);
        } catch (ActivityNotFoundException anfe) {
            showDialog(AdminActivity.this, "No Scanner Found", "Download a scanner code activity", "Yes", "No").show();
        }
    }

    public void ScanBarcodeDeleteProduct() {
        try {
            Intent intent = new Intent(Config.ACTION_SCAN);
            intent.putExtra("SCAN MODE", "PRODUCT_MODE");
            startActivityForResult(intent, 2);
        } catch (ActivityNotFoundException anfe) {
            showDialog(AdminActivity.this, "No Scanner Found", "Download a scanner code activity", "Yes", "No").show();
        }
    }

    private static AlertDialog showDialog(final Activity act, CharSequence title, CharSequence message, CharSequence buttonYes, CharSequence buttonNo) {
        AlertDialog.Builder downloadDialog = new AlertDialog.Builder(act);
        downloadDialog.setTitle(title);
        downloadDialog.setMessage(message);
        downloadDialog.setPositiveButton(buttonYes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Uri uri = Uri.parse("market://search?q=pname:" + "com.google.zxing.client.android");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                try {
                    act.startActivity(intent);
                } catch (ActivityNotFoundException anfe) {

                }
            }
        });
        downloadDialog.setNegativeButton(buttonNo, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        return downloadDialog.show();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {

        if (requestCode == 0) {
            if (resultCode == RESULT_OK) {
                String contents = intent.getStringExtra("SCAN_RESULT");
                String format = intent.getStringExtra("SCAN_RESULT_FORMAT");

                Intent passBarcodeDetail = new Intent(AdminActivity.this, AddProductActivity.class);

                passBarcodeDetail.putExtra("barcode", contents);
                passBarcodeDetail.putExtra("barcode_format", format);
                startActivity(passBarcodeDetail);
            }

        } else if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                String contents = intent.getStringExtra("SCAN_RESULT");
                String format = intent.getStringExtra("SCAN_RESULT_FORMAT");

                Intent passBarcodeDetail = new Intent(AdminActivity.this, EditProductActivity.class);

                passBarcodeDetail.putExtra("barcode", contents);
                passBarcodeDetail.putExtra("barcode_format", format);
                startActivity(passBarcodeDetail);
            }
        } else if (requestCode == 2) {
            if (resultCode == RESULT_OK) {
                final String contents = intent.getStringExtra("SCAN_RESULT");
                final String format = intent.getStringExtra("SCAN_RESULT_FORMAT");

                SharedPreferences printsharedPreferences = getSharedPreferences(Config.SHARED_PREF_ADMIN, Context.MODE_PRIVATE);
                final int shopID = printsharedPreferences.getInt(Config.SHOPID_ADMIN, -1);

                final String strShopID = String.valueOf(shopID);

                StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.DELETE_PRODUCT_URL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Toast.makeText(AdminActivity.this, response, Toast.LENGTH_LONG).show();
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(AdminActivity.this, error.toString(), Toast.LENGTH_LONG).show();
                            }
                        }) {
                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put(Config.KEY_PRODUCT_SHOP_ID, strShopID);
                        params.put(Config.KEY_BARCODE, contents);
                        params.put(Config.KEY_BARCODE_FORMAT, format);
                        return params;
                    }

                };

                RequestQueue requestQueue = Volley.newRequestQueue(this);
                requestQueue.add(stringRequest);

            }
        }

    }

    public void webDisplay(int shopID){
        WebView webView = (WebView) this.findViewById(R.id.content_admin_webview);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(Config.WEB_PRODUCT_DISPLAY_URL + shopID);
        webView.setWebViewClient(new WebViewClient());
        webView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {

                webViewProgressBar = (ProgressBar)findViewById(R.id.content_admin_web_progress_bar);
            }
        });

        webView.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {

                /** This prevents the loading of pages in system browser */
                return false;
            }

            /** Callback method, executed when the page is completely loaded */
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                LinearLayout linearLayout = (LinearLayout) findViewById(R.id.content_admin_linear_layout_2);
                linearLayout.setVisibility(View.GONE);
                webViewProgressBar.setVisibility(View.GONE);
                TextView loading = (TextView) findViewById(R.id.content_admin_tv_loading);
                loading.setVisibility(View.GONE);

            }

        });



    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.admin, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.nav_scan_product) {
            Intent intent = new Intent(AdminActivity.this, MainActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_edit_profile) {
            Intent intent = new Intent(AdminActivity.this, ManageProfileActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_add_product) {
            ScanBarcodeAddProduct();
        } else if (id == R.id.nav_edit_product) {
            ScanBarcodeEditProduct();
        } else if (id == R.id.nav_delete_product) {
            ScanBarcodeDeleteProduct();
        } else if (id == R.id.nav_logout) {
            logout();
        } else if (id == R.id.nav_manage_shop){
            Intent intent = new Intent(AdminActivity.this, ManageShopActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
