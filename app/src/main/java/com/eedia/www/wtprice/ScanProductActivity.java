package com.eedia.www.wtprice;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import java.text.DecimalFormat;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ScanProductActivity extends AppCompatActivity {

    TextView displayProductName;
    TextView displayProductPrice;
    TextView displayProductDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_product);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent getShopDetail = getIntent();
        String shopName = getShopDetail.getStringExtra("shop_name");
        String shopDescription = getShopDetail.getStringExtra("shop_description");

        TextView textViewShopName = (TextView) findViewById(R.id.content_scan_product_tv_display_shopname);
        TextView textViewShopDescription = (TextView) findViewById(R.id.content_scan_product_tv_shop_description);
        textViewShopName.setText(shopName);
        textViewShopDescription.setText(shopDescription);

        SharedPreferences sharedPreferences = ScanProductActivity.this.getSharedPreferences(Config.SHARED_PREF_SHOPNAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(Config.SHOP_NAME_SESSION, shopName);
        editor.apply();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                scanBar(view);
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void scanBar(View v) {
        try {
            Intent intent = new Intent(Config.ACTION_SCAN);
            intent.putExtra("SCAN MODE", "PRODUCT_MODE");
            startActivityForResult(intent, 0);
        } catch (ActivityNotFoundException anfe) {
            showDialog(ScanProductActivity.this, "No Scanner Found", "Download a scanner code activity", "Yes", "No").show();
        }
    }

    private static AlertDialog showDialog(final Activity act, CharSequence title, CharSequence message, CharSequence buttonYes, CharSequence buttonNo) {
        AlertDialog.Builder downloadDialog = new AlertDialog.Builder(act);
        downloadDialog.setTitle(title);
        downloadDialog.setMessage(message);
        downloadDialog.setPositiveButton(buttonYes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Uri uri = Uri.parse("market://search?q=pname:" + "com.google.zxing.client.android");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                try {
                    act.startActivity(intent);
                } catch (ActivityNotFoundException anfe) {

                }
            }
        });
        downloadDialog.setNegativeButton(buttonNo, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        return downloadDialog.show();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {

        if (requestCode == 0) {
            if (resultCode == RESULT_OK) {
                String contents = intent.getStringExtra("SCAN_RESULT");
                String format = intent.getStringExtra("SCAN_RESULT_FORMAT");
                Intent getShopDetail = getIntent();
                int shopID = getShopDetail.getIntExtra("shop_id", -1);

                String url = Config.GET_PRODUCT_DETAIL_URL + shopID + Config.GET_PRODUCT_DETAIL_URL_2 + contents + Config.GET_PRODUCT_DETAIL_URL_3 + format;

                StringRequest stringRequest = new StringRequest(url, new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        String productName = "";
                        String productDesc = "";
                        double productPrice = 0;


                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray result = jsonObject.getJSONArray(Config.JSON_ARRAY);
                            JSONObject getproductdetail = result.getJSONObject(0);
                            productName = getproductdetail.getString(Config.GET_PRODUCT_DETAIL_NAME);
                            productPrice = getproductdetail.getDouble(Config.GET_PRODUCT_DETAIL_PRICE);
                            productDesc = getproductdetail.getString(Config.GET_PRODUCT_DETAIL_DESC);

                            if(productName == ""){
                                Toast.makeText(ScanProductActivity.this, "Product", Toast.LENGTH_SHORT).show();
                            }

                            displayProductName = (TextView) findViewById(R.id.content_scan_product_tv_display_product_name);
                            displayProductPrice = (TextView) findViewById(R.id.content_scan_product_tv_display_product_price);
                            displayProductDescription = (TextView) findViewById(R.id.content_scan_product_tv_product_description);

                            DecimalFormat precision = new DecimalFormat("0.00");

                            displayProductName.setText(productName);
                            displayProductPrice.setText("RM " + precision.format(productPrice));
                            displayProductDescription.setText(productDesc);
                            
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(ScanProductActivity.this, error.getMessage().toString(), Toast.LENGTH_LONG).show();
                            }
                        });

                RequestQueue requestQueue = Volley.newRequestQueue(this);
                requestQueue.add(stringRequest);

            }
        }

    }

}
