
-- phpMyAdmin SQL Dump
-- version 2.11.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 14, 2016 at 10:15 PM
-- Server version: 5.1.57
-- PHP Version: 5.2.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `a8621098_WTPrice`
--

-- --------------------------------------------------------
--
--
-- Table structure for table `table_admin`
--
--
CREATE TABLE `table_admin` (
  `admin_id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_username` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `admin_password` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `admin_email` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `admin_fname` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `admin_lname` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `shop_id` int(11) NOT NULL,
  PRIMARY KEY (`admin_id`),
  UNIQUE KEY `admin_email` (`admin_email`),
  UNIQUE KEY `admin_username` (`admin_username`),
  UNIQUE KEY `shop_id` (`shop_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=0 ;

-- --------------------------------------------------------

--
-- Table structure for table `table_product`
--

CREATE TABLE `table_product` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_name` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `product_price` float NOT NULL,
  `product_description` varchar(1000) COLLATE latin1_general_ci DEFAULT NULL,
  `product_barcode` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `product_barcode_format` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `shop_id` int(11) NOT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=0 ;

-- --------------------------------------------------------

--
-- Table structure for table `table_shop`
--

CREATE TABLE `table_shop` (
  `shop_id` int(11) NOT NULL AUTO_INCREMENT,
  `shop_name` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `shop_description` varchar(500) COLLATE latin1_general_ci DEFAULT NULL,
  `shop_url` varchar(500) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`shop_id`),
  UNIQUE KEY `shop_name` (`shop_name`),
  UNIQUE KEY `shop_url` (`shop_url`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=0 ;

