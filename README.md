# WTPrice #

This project is for any store to implement barcode scanner system easily. 
This system reduce the hassle of buying embedded barcode scanner for their shop which are expensive and not upgradable.

To use this application, each shop must has their own QR code and all the item they sold need to have barcode.

Module
1. Register your shop.
2. CRUD operation on product using barcode scanner.
3. User can scan the products.

Technology Stack
1. Database - MySQL
2. Backend - PHP
3. Mobile - Java Android
4. Connection to database - Volley

Setup Web Service
1. Import the sql file into your server. This step can be done using PHPMyAdmin.
2. Open the folder called WTPrice inside the source code and edit the conn.php to connect to the database.
3. Copy all the content of the folder WTPrice to your app server usually in a folder called htdocs or www.

Setup Android Project
1. Open the source code inside Android studio
2. Edit the url inside the Config.java to match web server url.
3. Run or generate the apk on your Android phone.
